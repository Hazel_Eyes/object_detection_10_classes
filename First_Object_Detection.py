import numpy as np
import argparse
import imutils
from imutils.video import VideoStream
from imutils.video import FPS
import time 
import cv2 as cv


#Program:
#Możliwość dodania z linii poleceń pliku z siecią neuronową do programu, odpowiednio po -- trzeba dodać scieżkę do pliku
vArgPrs = argparse.ArgumentParser()
vArgPrs.add_argument("-p", "--prototxt", required=True, help="Ścieżka do pliku prototxt path to Caffe 'deploy' prototxt file")
vArgPrs.add_argument("-m", "--model", required=True, help="Ścieżka do modelu sieci neuronowejpath to Caffe pre-trained model")
args = vars(vArgPrs.parse_args())

#Klasy obiektów  w danej sieci neuronowej, do
vObjectClasses = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]

#Poziom, od ktorego obiekt jest klasyfikowany
gCONFIDENCE = 0.2

vColors = np.random.uniform(0, 255, size=(len(vObjectClasses), 3))

vNeuronNet = cv.dnn.readNetFromCaffe(args["prototxt"], args["model"])

#Src 0 - z domyślnie wmontowanej kamery, jeżeli są inne trzeba dać inną liczbę(np 1 jeżeli chcemy przekazywać obraz z drugiej kamery, 3 -trzeciej itp)
vVideo = VideoStream(src=0).start()
time.sleep(2.0)
fps = FPS().start()

#Wczytywanie ramek z video
while True:
	vFrame = vVideo.read()
	vFrame = imutils.resize(vFrame, width=400)
	(h, w) = vFrame.shape[:2]
	vBlob = cv.dnn.blobFromImage(cv.resize(vFrame, (300, 300)),
		0.007843, (300, 300), 127.5)

	vNeuronNet.setInput(vBlob)
	vDetections = vNeuronNet.forward()

	for i in np.arange(0, vDetections.shape[2]):
		vConfidence = vDetections[0, 0, i, 2]

        #Jeżeli prawdopodobieństwo jest większe dla konkretnego obiektu niż założony próg
		if vConfidence > gCONFIDENCE:
			idx = int(vDetections[0, 0, i, 1])
			box = vDetections[0, 0, i, 3:7] * np.array([w, h, w, h])
			(startX, startY, endX, endY) = box.astype("int")

			#Label dla poszczególnego obiektu
			vLabel = "{}: {:.2f}%".format(vObjectClasses[idx], vConfidence * 100)
			cv.rectangle(vFrame, (startX, startY), (endX, endY), vColors[idx], 2)
			y = startY - 15 if startY - 15 > 15 else startY + 15
			# Rysowanie prawdopodobieństwa przy ramce obrazu
			cv.putText(vFrame, vLabel, (startX, y), cv.FONT_HERSHEY_SIMPLEX, 0.5, vColors[idx], 2)
	#Jedna ramka obrazu przekazywana do okna
	cv.imshow("Okno", vFrame)
	key = cv.waitKey(1) & 0xFF
	#Q - konczy dzialanie programu
	if key == ord('q'):
		break

	fps.update()